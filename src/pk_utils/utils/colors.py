from typing import List, Dict

import seaborn as sns


def get_qualitatively_distinct_colors(n_colors: int) -> List[str]:
    """
    It is by far not trivial to get a long set of qualitatively distinct colors. Here is a solution
    that works not too bad for a number of at most 31 colors. Please use with care if n_colors > 31.

    Returns iterable of hex color strings.
    """
    standard_palette = sns.color_palette("Paired").as_hex() + sns.color_palette("Set2").as_hex()

    if n_colors > len(standard_palette):
        darker_husl_palette = sns.husl_palette(n_colors - len(standard_palette), l=0.5).as_hex()
        return standard_palette + darker_husl_palette
    else:
        return standard_palette[:n_colors]


def get_color_dictionary(names: List[str]) -> Dict:
    """Returns dictionary with keys = `names` and a selected set of distinct colors as values."""
    colors = get_qualitatively_distinct_colors(len(names))
    color_dict = {}
    for name, color in zip(names, colors):
        color_dict[name] = color
    return color_dict
